<%-- 
    Document   : registrationSuccess
    Created on : 21-Feb-2016, 6:52:12 PM
    Author     : Linh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registration</title>
    </head>
    <body>
        <div id="wrapper">
        <jsp:include page="../include/header.jsp" />
        <main>
            <div id="template">
                <h3>your registration is successful. Please go to your email to activate your account</h3>
            </div>
        </main>
        <jsp:include page="../include/footer.jsp" />
        </div>
    </body>
</html>
