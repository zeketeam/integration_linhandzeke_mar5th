<%-- 
    Document   : leaderboard
    Created on : 25-Feb-2016, 11:15:48 PM
    Author     : ma
--%>

<%@page import="java.util.Collection"%>
<%@page import="java.util.Collections"%>
<%@page import="java.util.LinkedList"%>
<%@page import="model.Player"%>
<%@page import="entities.Leaderboard"%>
<%@page import="entities.User"%>
<%@page import="java.util.HashSet"%>
<%@page import="java.util.Set"%>
<%@page import="entities.Pavilion"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Leader Board</title>
           <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
           <link rel="stylesheet" type="text/css" href="resources/css/bootstrap.min.css" />
            <script type="text/javascript">
            
            function PickPlayer(){
            $.get("${pageContext.request.contextPath}/leaderboard.htm",
            {
            selected : $("#players option:selected").text()
            },
            function(data,status){
            // alert("Data: " + data + "\nStatus: " + status);
            location.reload();
            });
            }
            
            $(document).ready(function() {
                
            });   
           </script>
           <style>
               .gap{
                   height: 50px;
               }    
           </style>
    </head>
    
    <body id="body">
     <div class="row">
     
      <div class="row col-md-12"> 
      <h1>Hi ${user.getName()} Your Score is : ${message}</h1>
      </div>
      
      
      <div class="row col-md-12 ">
       <div class="col-md-6 text-center" >
          <table class="table">
           <tr>
           <th>User Name</th>
           <th>Points</th>
           </tr>
        <%
           Leaderboard lb = (Leaderboard)request.getSession().getAttribute("lb");
           Set pgroups = lb.getPavilions();
           Set users = new HashSet<User>();
           LinkedList list = new LinkedList();
           Pavilion pavilionTmp;
           User userTmp;
           LinkedList<Player> teamA;
           LinkedList<Player> teamB;
           int rank = 0;
            for(Object p : pgroups){
                pavilionTmp = (Pavilion) p;
                users = pavilionTmp.getUsers();              
                
                for(Object u : users){
                userTmp = (User)u;
                rank +=1;
                //out.println(userTmp.getName());*/
        %>
            <tr>
                <td><%=userTmp.getName()%></td>
                <td><%if(userTmp.getId()==1){%> ${message}<%}else{%> 0 <%}%></td>
            </tr>
        <%}
        }%>
       </table>
       </div>
       
       <div id="playersPanel" class="row">
        <div class="row col-md-6 text-center">
           <select id="players">
            <optgroup label="Team A">
            <%
                teamA = (LinkedList<Player>)request.getSession().getAttribute("teamA");
                for(Player a : teamA){
            %>
            <option value="<%=a.getName()%>" <%if(a.getSelected()!=0){%>selected="selected"<%}%> ><%=a.getName()%></option>
            <%
            }
            %>    
            </optgroup>
            <optgroup label="Team B">
            <%
                teamB = (LinkedList<Player>)request.getSession().getAttribute("teamB");
                for(Player a : teamB){
            %>
                <option value="<%=a.getName()%>" <%if(a.getSelected()!=0){%>selected="selected"<%}%>><%=a.getName()%></option>
            <%
                }
            %>
            </optgroup>
            </select>
        </div>
            <div class="gap"></div>
        <div class="row col-md-6 text-center">
            <input type="button" id="launch" value="Launch" onclick="PickPlayer();" /></div>
       </div>  
      </div> <!--end for leader board-->
      
      
      <div class="row">
      <div id="teamA" class="col-md-6">
          <table class="table">
            <thead>Team A</thead>
            <tr>
            <th>Player Name</th>
            <th>Game Points</th>
            </tr>
        <%
           teamA = (LinkedList<Player>)request.getSession().getAttribute("teamA");
          
            for(Player a : teamA){
        %>
        <tr>
           <td><%=a.getName()%></td>
           <td><%=a.getPoints()%></td>
        </tr>   
        <%
        }
        %>
       </table>
      </div>
      <div id="teamB" class="col-md-6">
          <table class="table">
          <thead>Team B</thead>
            <tr>
            <th>Player Name</th>
            <th>Game Points</th>
            </tr> 
       <%
           teamB = (LinkedList<Player>)request.getSession().getAttribute("teamB");
           for(Player b : teamB){
       %>   
            <tr>
           <td><%=b.getName()%></td>
           <td><%=b.getPoints()%></td>
        </tr>  
       <%
       }
       %>
       </table>
      </div>
      </div>
     </div><!--whole site-->
    </body>
    
            
</html>
