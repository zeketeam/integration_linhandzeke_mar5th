<%-- 
    Document   : userFrofileIndex
    Created on : 2016-2-28, 7:16:20
    Author     : Ziyi
--%>

<%@page import="entities.User"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">        
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" />
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css" />
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" />
        <script src="https://code.jquery.com/jquery-1.11.3.js"></script>
        <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <title>User profile</title>
        <style>
            .txtVM{
                /*background-color: grey;*/
            }
        </style>
    </head>
    <body>
        
        <div id="wrapper">
        <jsp:include page="../include/header.jsp" />
        <main>
            <h2>User Profile Overview</h2>
            <script>
                if("${msg}"!="")
                    alert("${msg}");
            </script>
            <div class="row">                 
                    <div class="col-md-9">                        
                        <div id="pfEditFormZZ">                    
                            <form class="form-horizontal" 
                                  method="post" 
                                  action="userProfileEdit.htm">
                                <h2 id="formTitleZZ"><small>Personal Information:</small></h2>
                                <br/>
                                <div class="form-group">                            
                                    <label for="name" class=" txtVM control-label col-xs-4">First name</label>
                                    <div class="control-label col-xs-5 txtVM" style="">
                                        ${currentUser.getName()}
                                    </div>
                                </div>
                                <div class="form-group">                            
                                    <label for="name" class="control-label col-xs-4">Last name</label>
                                    <div class="control-label col-xs-5 txtVM">
                                        Smith
                                    </div>
                                </div>
                                <div class="form-group">                            
                                    <label for="name" class="control-label col-xs-4">Gender</label>
                                    <div class="control-label col-xs-5 txtVM">
                                        Male
                                    </div>
                                        
                                </div>
                                <div class="form-group">                            
                                    <label for="name" class="control-label col-xs-4">Birthday</label>
                                    <div class="control-label col-xs-5">
                                        1990/01/01
                                    </div>
                                </div>
                                <div class="form-group">                            
                                    <label for="name" class="control-label col-xs-4">Language</label>
                                    <div class="control-label col-xs-5">
                                        English
                                    </div>
                                </div>
                                <div class="form-group">                            
                                    <label for="name" class="control-label col-xs-4">Status</label>
                                    <div class="control-label col-xs-5">
                                        Online
                                    </div>
                                </div>
                                <hr style="display: block;
                                    height: 1px;
                                    border: 0;
                                    border-top: 1px solid #ccc;
                                    margin: 1em 0;
                                    padding: 0;"/>
                                <h2 id="formTitleZZ"><small>Contact Information:</small></h2>
                                <br/>
                                <h5>We use your contact details to deliver important information about your account. 
                                It also helps friends to find you on CricketHeros.</h5>
                                <br/>
                                <div class="form-group">                            
                                    <label for="name" class="control-label col-xs-4">Email address</label>
                                    <div class="control-label col-xs-5">
                                        ${currentUser.getEmail()}
                                    </div>
                                </div>                        
                                <div class="form-group">
                                    <label for="name" class="control-label col-xs-4">Mobile phone</label>
                                    <div class="control-label col-xs-5">
                                        ${currentUser.getPhone()}
                                    </div>
                                </div>
                                <div class="form-group">                            
                                    <label for="name" class="control-label col-xs-4">Home phone</label>
                                    <div class="control-label col-xs-5">
                                        +1-416-724-8374
                                    </div>
                                </div>
                                <div class="form-group">                            
                                    <label for="name" class="control-label col-xs-4">Office phone</label>
                                    <div class="control-label col-xs-5">
                                        +1-605-729-8284
                                    </div>
                                </div>
                                <div class="form-group">                            
                                    <label for="name" class="control-label col-xs-4">Address</label>
                                    <div class="control-label col-xs-5">
                                         ${currentUser.getAddress()}
                                    </div>
                                </div>
                                <div class="form-group">                            
                                    <label for="name" class="control-label col-xs-4">City</label>
                                    <div class="control-label col-xs-5">
                                        Toronto
                                    </div>
                                </div>
                                <div class="form-group">                            
                                    <label for="name" class="control-label col-xs-4">State/province</label>
                                    <div class="control-label col-xs-5">
                                        Ontario
                                    </div>
                                </div>
                                <div class="form-group">                            
                                    <label for="name" class="control-label col-xs-4">Country/region</label>
                                    <div class="control-label col-xs-5">
                                        Canada
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="control-label col-xs-4">Postcode</label>
                                    <div class="control-label col-xs-5">
                                        U7R 4M6
                                    </div>
                                </div>
                                <hr style="display: block;
                                    height: 1px;
                                    border: 0;
                                    border-top: 1px solid #ccc;
                                    margin: 1em 0;
                                    padding: 0;"/>
                                <h2 id="formTitleZZ"><small>Security Options:</small></h2>
                                <br/>
                                <div class="form-group">
                                    <label for="name" class="control-label col-xs-4">Password</label>
                                    <div class="control-label col-xs-5">
                                        ******
                                    </div>
                                </div>
                                
                                

                                <div class="form-group">
                                    <label for="btnInsert" class="control-label col-xs-4"></label>
                                    <div class="col-xs-5">
                                        <input type="submit" class="btn btn-default btn-block" name="submit" value="Edit my profile">            
                                    </div>
                                </div>
                            </form>
                        </div>
                        
                    </div>
                    <div style="float: right;
                        margin: 0 0 0px 0px;">
                        <br/>
                        <img src="resources/images/photo.png"/><br/>
                        User Photo
                        <br/>
                        <br/>                        
                        CricketHero ID:&nbsp; <small>0179</small>
                        <br/>
                        Pavilion ID:&nbsp;  <small>034</small>
                        <br/>
                        Date joined:&nbsp;  <small>2016/02/28</small>
                        <br/>
                    </div>
                </div>
            
        </main>
        <jsp:include page="../include/footer.jsp" />
        </div>
    </body>
</html>
