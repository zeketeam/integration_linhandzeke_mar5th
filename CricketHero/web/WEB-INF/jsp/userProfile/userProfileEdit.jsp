<%-- 
    Document   : userFrofileIndex
    Created on : 2016-2-28, 7:16:20
    Author     : Ziyi
--%>

<%@page import="model.UserDAO"%>
<%@page import="entities.User, org.springframework.beans.factory.annotation.Autowired"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">        
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" />
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css" />
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" />
        <script src="https://code.jquery.com/jquery-1.11.3.js"></script>
        <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <title>User profile edit</title>
    </head>
    <body>
        <% %>
        <div id="wrapper">
        <jsp:include page="../include/header.jsp" />
        <main> 
            <h2>User Profile Editing</h2>
            <div class="row">                 
                    <div class="col-md-7">                        
                        <div id="pfEditFormZZ">
                
                            <form:form class="form-horizontal"
                                       commandName="updatedUser"
                                       modelAttribute="updatedUser"
                                       method="post" 
                                       action="userProfileResult.htm">
                                <h2 id="formTitleZZ"><small>Personal Information:</small></h2>
                                <br/>
                                <div class="form-group">                            
                                    <label for="name" class="control-label col-xs-4">First name</label>
                                    <div class="col-xs-5">
                                        <form:input path="name" class="form-control" name="fn" id="fn" value="${currentUser.getName()}" />                                        
                                    </div>
                                </div>
                                <div class="form-group">                            
                                    <label for="name" class="control-label col-xs-4">Last name</label>
                                    <div class="col-xs-5">
                                        <input type="text" class="form-control" name="ln" id="ln" value="Smith" >
                                    </div>
                                </div>
                                <div class="form-group">                            
                                    <label for="name" class="control-label col-xs-4">Gender</label>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <input type="radio" name="gender" id="gender" value="male" checked>
                                            Male
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <input type="radio" name="gender" id="gender" value="female">
                                          Female
                                        
                                </div>
                                <div class="form-group">                            
                                    <label for="name" class="control-label col-xs-4">Birthday</label>
                                    <div class="col-xs-5">
                                        <input type="text" class="form-control" name="birth" id="birth" value="1990/01/01" >
                                    </div>
                                </div>
                                <div class="form-group">                            
                                    <label for="name" class="control-label col-xs-4">Language</label>
                                    <div class="col-xs-5">
                                        <input type="text" class="form-control" name="lang" id="lang" value="English" >
                                    </div>
                                </div>
                                <div class="form-group">                            
                                    <label for="name" class="control-label col-xs-4">Status</label>
                                    <div class="col-xs-5">
                                        <input type="text" class="form-control" name="status" id="status" value="Online" >
                                    </div>
                                </div>
                                <hr style="display: block;
                                    height: 1px;
                                    border: 0;
                                    border-top: 1px solid #ccc;
                                    margin: 1em 0;
                                    padding: 0;"/>
                                <h2 id="formTitleZZ"><small>Contact Information:</small></h2>
                                <br/>
                                <h5>We use your contact details to deliver important information about your account. 
                                It also helps friends to find you on CricketHeros.</h5>
                                <br/>
                                <div class="form-group">                            
                                    <label for="name" class="control-label col-xs-4">Email address</label>
                                    <div class="col-xs-5">
                                        <form:input path="email" class="form-control" name="email" id="email" value="${currentUser.getEmail()}" />
                                    </div>
                                </div>                        
                                <div class="form-group">
                                    <label for="name" class="control-label col-xs-4">Mobile phone</label>
                                    <div class="col-xs-5">
                                        <form:input path="phone" type="text" class="form-control" name="phone" id="phone" value="${currentUser.getPhone()}" />
                                    </div>
                                </div>
                                <div class="form-group">                            
                                    <label for="name" class="control-label col-xs-4">Home phone</label>
                                    <div class="col-xs-5">
                                        <input type="text" class="form-control" name="homePhone" id="homePhone" value="+1-416-724-8374" >
                                    </div>
                                </div>
                                <div class="form-group">                            
                                    <label for="name" class="control-label col-xs-4">Office phone</label>
                                    <div class="col-xs-5">
                                        <input type="text" class="form-control" name="officePhone" id="officePhone" value="+1-605-729-8284" >
                                    </div>
                                </div>
                                <div class="form-group">                            
                                    <label for="name" class="control-label col-xs-4">Address</label>
                                    <div class="col-xs-5">
                                        <form:input path="address" class="form-control" name="addr" id="addr" value="${currentUser.getAddress()}" />
                                    </div>
                                </div>
                                <div class="form-group">                            
                                    <label for="name" class="control-label col-xs-4">City</label>
                                    <div class="col-xs-5">
                                        <input type="text" class="form-control" name="city" id="city" value="Toronto" >
                                    </div>
                                </div>
                                <div class="form-group">                            
                                    <label for="name" class="control-label col-xs-4">State/province</label>
                                    <div class="col-xs-5">
                                        <input type="text" class="form-control" name="province" id="province" value="Ontario" >
                                    </div>
                                </div>
                                <div class="form-group">                            
                                    <label for="name" class="control-label col-xs-4">Country/region</label>
                                    <div class="col-xs-5">
                                        <input type="text" class="form-control" name="country" id="country" value="Canada" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="control-label col-xs-4">Postcode</label>
                                    <div class="col-xs-5">
                                        <input type="text" class="form-control" name="postcode" id="postcode" value="U7R 4M6" >
                                    </div>
                                </div>
                                <hr style="display: block;
                                    height: 1px;
                                    border: 0;
                                    border-top: 1px solid #ccc;
                                    margin: 1em 0;
                                    padding: 0;"/>
                                <h2 id="formTitleZZ"><small>Security Options:</small></h2>
                                <br/>
                                <div class="form-group">
                                    <label for="name" class="control-label col-xs-4">Password</label>
                                    <div class="col-xs-5">
                                        <form:password class="form-control" name="pw" id="pw" path="pwd" />
                                        
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="control-label col-xs-4">Confirm password</label>
                                    <div class="col-xs-5">
                                        <input type="password" class="form-control" name="cfmPw" id="cfmPw" placeholder="" >
                                    </div>
                                </div>
                                

                                <div class="form-group">
                                    <label for="btnInsert" class="control-label col-xs-4"></label>
                                    <div class="col-xs-5">
                                        <input type="submit" class="btn btn-default btn-block" name="submit" value="Submit">            
                                    </div>
                                </div>
                            </form:form>
                        </div>
                        
                    </div>
                    <div style="float: right;
                        margin: 0 0 0px 0px;">
                        <img src="resources/images/photo.png"/><br/>
                        <div class="form-group">
                            <label for="exampleInputFile">Upload image</label>
                            <input type="file" id="">
                            <p class="help-block">Please upload your new profile picture here.</p>
                          </div>                                              
                        <br/>
                        <br/>                        
                        CricketHero ID:&nbsp; 0179
                        <br/>
                        Pavilion ID:&nbsp;  034
                        <br/>
                        Date joined:&nbsp;  2016/02/28
                        <br/>
                    </div>
                </div>
            
        </main>
        <jsp:include page="../include/footer.jsp" />
        </div>
    </body>
</html>
