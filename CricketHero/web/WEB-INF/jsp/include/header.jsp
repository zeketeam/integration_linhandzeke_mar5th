<%-- 
    Document   : header
    Created on : 25-Feb-2016, 6:17:05 PM
    Author     : Linh
--%>
<%@page import="entities.User"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="resources/css/main.css" type="text/css" rel="stylesheet"/>
        <title>Cricket Hero</title>
    </head>
    <body>
        <%  
           String email="";
           User user = (User)session.getAttribute("user");
           if(session.getAttribute("user")!= null){ 
               email = user.getEmail();
           }
        %>           
        <div class="userInfo">
            <% if(email.equals("")){ %>
                <a href="signup.htm">Sign Up</a> | <a href="login.htm">Login</a>
            <% }else{ %>
                <%= user.getEmail() + " " %><a href="logout.htm">LogOut</a>
            <% } %>
        </div>
        <header id="top">          
            <img id="logo" src="resources/images/logo.jpg">
            <h1>CRICKET HERO</h1>
	</header>
	<nav>
            <ul>
                <li><a href="index.htm" class="active"><img  src="resources/images/home.png" alt="Home" class="home"/></a></li>
                <li><a href="Star.html">Star</a></li>
                <li><a href="#">Music</a></li>
                <li><a href="#">Movies</a></li>
                <li><a href="#">Sports</a></li>
                <li><a href="#">Society</a></li>
                <li><a href="#">Health</a></li>
                <li><a href="userProfileIndex.htm">User Profile</a></li>
            </ul>
	</nav>
    </body>
</html>


