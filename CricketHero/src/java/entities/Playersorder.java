package entities;
// Generated 26-Feb-2016 3:07:17 AM by Hibernate Tools 4.3.1


import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Playersorder generated by hbm2java
 */
@Entity
@Table(name="playersorder"
    ,catalog="mydb"
)
public class Playersorder  implements java.io.Serializable {


     private PlayersorderId id;
     private Game game;
     private Players players;
     private Integer order;

    public Playersorder() {
    }

	
    public Playersorder(PlayersorderId id, Game game, Players players) {
        this.id = id;
        this.game = game;
        this.players = players;
    }
    public Playersorder(PlayersorderId id, Game game, Players players, Integer order) {
       this.id = id;
       this.game = game;
       this.players = players;
       this.order = order;
    }
   
     @EmbeddedId

    
    @AttributeOverrides( {
        @AttributeOverride(name="playerId", column=@Column(name="playerId", nullable=false) ), 
        @AttributeOverride(name="gameId", column=@Column(name="gameId", nullable=false) ) } )
    public PlayersorderId getId() {
        return this.id;
    }
    
    public void setId(PlayersorderId id) {
        this.id = id;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="gameId", nullable=false, insertable=false, updatable=false)
    public Game getGame() {
        return this.game;
    }
    
    public void setGame(Game game) {
        this.game = game;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="playerId", nullable=false, insertable=false, updatable=false)
    public Players getPlayers() {
        return this.players;
    }
    
    public void setPlayers(Players players) {
        this.players = players;
    }

    
    @Column(name="order")
    public Integer getOrder() {
        return this.order;
    }
    
    public void setOrder(Integer order) {
        this.order = order;
    }




}


