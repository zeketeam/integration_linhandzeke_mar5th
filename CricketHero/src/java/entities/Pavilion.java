package entities;
// Generated 26-Feb-2016 3:07:17 AM by Hibernate Tools 4.3.1


import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Pavilion generated by hbm2java
 */
@Entity
@Table(name="pavilion"
    ,catalog="mydb"
)
public class Pavilion  implements java.io.Serializable {


     private Integer id;
     private Leaderboard leaderboard;
     private String name;
     private String active;
     private String status;
     private String description;
     private String maxUsers;
     private Date dateCreated;
     private Date dateModified;
     private Set<User> users = new HashSet<User>(0);

    public Pavilion() {
    }

    public Pavilion(Leaderboard leaderboard, String name, String active, String status, String description, String maxUsers, Date dateCreated, Date dateModified, Set<User> users) {
       this.leaderboard = leaderboard;
       this.name = name;
       this.active = active;
       this.status = status;
       this.description = description;
       this.maxUsers = maxUsers;
       this.dateCreated = dateCreated;
       this.dateModified = dateModified;
       this.users = users;
    }
   
     @Id @GeneratedValue(strategy=IDENTITY)

    
    @Column(name="id", unique=true, nullable=false)
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="leaderBoardId")
    public Leaderboard getLeaderboard() {
        return this.leaderboard;
    }
    
    public void setLeaderboard(Leaderboard leaderboard) {
        this.leaderboard = leaderboard;
    }

    
    @Column(name="name", length=45)
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    
    @Column(name="active", length=45)
    public String getActive() {
        return this.active;
    }
    
    public void setActive(String active) {
        this.active = active;
    }

    
    @Column(name="status", length=45)
    public String getStatus() {
        return this.status;
    }
    
    public void setStatus(String status) {
        this.status = status;
    }

    
    @Column(name="description", length=45)
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }

    
    @Column(name="maxUsers", length=45)
    public String getMaxUsers() {
        return this.maxUsers;
    }
    
    public void setMaxUsers(String maxUsers) {
        this.maxUsers = maxUsers;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="dateCreated", length=19)
    public Date getDateCreated() {
        return this.dateCreated;
    }
    
    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="dateModified", length=19)
    public Date getDateModified() {
        return this.dateModified;
    }
    
    public void setDateModified(Date dateModified) {
        this.dateModified = dateModified;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="pavilion")
    public Set<User> getUsers() {
        return this.users;
    }
    
    public void setUsers(Set<User> users) {
        this.users = users;
    }




}


