package entities;
// Generated 26-Feb-2016 3:07:17 AM by Hibernate Tools 4.3.1


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Playersscoring generated by hbm2java
 */
@Entity
@Table(name="playersscoring"
    ,catalog="mydb"
)
public class Playersscoring  implements java.io.Serializable {


     private int playerId;
     private int gameId;
     private Integer scores;
     private Integer wickets;
     private Integer catches;
     private Integer stumps;

    public Playersscoring() {
    }

	
    public Playersscoring(int playerId, int gameId) {
        this.playerId = playerId;
        this.gameId = gameId;
    }
    public Playersscoring(int playerId, int gameId, Integer scores, Integer wickets, Integer catches, Integer stumps) {
       this.playerId = playerId;
       this.gameId = gameId;
       this.scores = scores;
       this.wickets = wickets;
       this.catches = catches;
       this.stumps = stumps;
    }
   
     @Id 

    
    @Column(name="playerId", unique=true, nullable=false)
    public int getPlayerId() {
        return this.playerId;
    }
    
    public void setPlayerId(int playerId) {
        this.playerId = playerId;
    }

    
    @Column(name="gameId", nullable=false)
    public int getGameId() {
        return this.gameId;
    }
    
    public void setGameId(int gameId) {
        this.gameId = gameId;
    }

    
    @Column(name="scores")
    public Integer getScores() {
        return this.scores;
    }
    
    public void setScores(Integer scores) {
        this.scores = scores;
    }

    
    @Column(name="wickets")
    public Integer getWickets() {
        return this.wickets;
    }
    
    public void setWickets(Integer wickets) {
        this.wickets = wickets;
    }

    
    @Column(name="catches")
    public Integer getCatches() {
        return this.catches;
    }
    
    public void setCatches(Integer catches) {
        this.catches = catches;
    }

    
    @Column(name="stumps")
    public Integer getStumps() {
        return this.stumps;
    }
    
    public void setStumps(Integer stumps) {
        this.stumps = stumps;
    }




}


