/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import entities.User;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.util.DigestUtils;


/**
 *
 * @author Linh
 */
public class UserDAO extends BaseDAO<User,Integer>{
    public boolean checkUserLogin(User user){
        Session session = getSessionFactory().openSession();
        String sql = "select u.* from User u where u.email = :username and u.pwd = :password and u.active=1 and u.status=1";
        Query query = session.createSQLQuery(sql).addEntity("User",User.class);
        if (query != null){
            query.setString("username",user.getEmail());
            query.setString("password",DigestUtils.md5DigestAsHex(user.getPwd().getBytes()));
            List result = query.list();
            if(!result.isEmpty()){
                user = (User)result.get(0);
                return true;
            }
        }
        return false;
    }
    
    public User checkActivationUser(String email,String ActivateCode){
        Criterion[]  cr = new Criterion[2] ;
        cr[0]=Restrictions.eq("active", ActivateCode);
        cr[1]=Restrictions.eq("email", email);
        List<User> users = findByCriteria(cr);
        if(users.isEmpty())
            return null;
        return users.get(0);
    }
    
    public boolean checkUserIsExsit(String email){
        if(getBy("email", email) == null)
            return false;
        return true;
    }
   
}
