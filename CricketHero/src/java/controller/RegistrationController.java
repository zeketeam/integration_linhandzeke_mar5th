/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import Utilities.Utilities;
import entities.User;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import model.UserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Linh
 */
@Controller
public class RegistrationController {
    @Autowired private UserDAO userDAO;
   
    @RequestMapping(value="/signup.htm")
    public String load(ModelMap modelMap) {
        modelMap.put("user", new User());
        return "user/registration";
    }
    @RequestMapping(value="/registrationSuccess.htm", method = RequestMethod.POST)
    public String registration(HttpServletRequest request, ModelMap modelMap, User user){
        String error = validate(user, request.getParameter("retypePassword"),request.getParameter("disclaimer"));
        
        if("".equals(error)){
            String code = activeCode();
            user.setPwd(DigestUtils.md5DigestAsHex(user.getPwd().getBytes()));
            user.setActive(code);
            user.setStatus("1");
            user.setDateCreated(new Date());
            userDAO.save(user);
            String message = "Thank you for your registration. Please click the link below to activate your account \n";
            message += "http://" + request.getServerName() + ":" + request.getLocalPort() + request.getContextPath() + "/activate.htm?a=" + user.getEmail() + "&c=" + code;
            try {
                Utilities.sendEmail(user.getEmail(),"Activate Your Account",message);
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(RegistrationController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (MessagingException ex) {
                Logger.getLogger(RegistrationController.class.getName()).log(Level.SEVERE, null, ex);
            }
            return "user/registrationSuccess";
        }
        
        modelMap.addAttribute("error", error);
        return "user/registration";
    }
    
    @RequestMapping(value="/activate.htm")
    public String activate(HttpServletRequest request,ModelMap modelMap) {
        User user = userDAO.checkActivationUser(request.getParameter("a"), request.getParameter("c"));
        if(user != null){
            user.setActive("1");
            userDAO.saveOrUpdate(user);
            request.getSession().setAttribute("user", user);
            return "user/activationConfirm";
        }
        return "index";
    }
    private String activeCode() {
      SecureRandom random = new SecureRandom();
      return new BigInteger(130, random).toString(30);
    }
    
    private String validate(User user, String reTypePassword, String disclaimer){
        String error = "";
        if("".equals(user.getEmail())){
            error += "Please enter your email<br/>";
        }else{
            if(!Utilities.EmailValidator(user.getEmail())){
                error += "This email is invalid. Please try another email<br/>";
            }else{
                if(userDAO.checkUserIsExsit(user.getEmail())){
                    error += "This email is not available. Please try another email<br/>";
                }
            }
        }
        
        if("".equals(user.getPwd())){
            error += "Please enter your password<br/>";
        }else{
            if("".equals(reTypePassword)){
                error += "Please enter your retype password<br/>";
            }else{
                if(!reTypePassword.equals(user.getPwd())){
                     error += "Password is not match with retype password<br/>";
                }
            }           
        }
        
        if(disclaimer == null){
            error += "Please Accept disclaimer<br/>";
        }
        
        return error;
    }
 
}
