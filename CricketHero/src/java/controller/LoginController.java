/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import entities.User;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import model.UserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Linh
 */
@Controller
public class LoginController{ 

    @Autowired private UserDAO userDAO;
   
    @RequestMapping(value="/login.htm", method = RequestMethod.GET)
    public String login(HttpServletRequest req, ModelMap modelMap) {
        
        
        modelMap.put("user", new User());
        return "user/login";
    }
    @RequestMapping(value="/loginConfirm.htm", method = RequestMethod.POST)
    public String loginConfirm(HttpServletRequest request, ModelMap modelMap, User user) {
        if(userDAO.checkUserLogin(user)){
            //zeke begin
            List<User> ul = (List<User>)userDAO.getBy("email", user.getEmail());
            user = ul.get(0);           
            //zeke end
            request.getSession().setAttribute("user", user);
            return "index";
        }
        modelMap.put("error","Username or Password is invalid");
        return "user/login";
    }
    
    @RequestMapping(value="/logout.htm", method = RequestMethod.GET)
    public String logout(HttpServletRequest request,ModelMap modelMap) {
        request.getSession().removeAttribute("user");
        return "index";
    }
    
    @RequestMapping(value="/forgetpwd.htm", method = RequestMethod.GET)
    public String forgetpwd(HttpServletRequest request,ModelMap modelMap) {
        return "forgetpwd";
    }
    
}
