/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import entities.Leaderboard;
import entities.Pavilion;
import entities.User;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.BaseDAO;
import model.Player;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author ma
 */
@Controller
public class LeaderBoardController {
    private HttpServletRequest hsr;
    
    
    @Autowired private BaseDAO dao;
    
        @RequestMapping(value="/leaderboard.htm" , method = RequestMethod.GET)
        public ModelAndView handleRequest(HttpServletRequest hsr, HttpServletResponse hsr1) throws Exception {
        //Hard code an user logged in
        ModelAndView mv = new ModelAndView("game/leaderboard");
        hsr = hsr;
        User user = (User) dao.getById(1);
        String pDescriptiono = "This group belong to User whose Id is 1";
        String pName = "Team Zeke";
        //James
        User james = (User) dao.getById(3);
        //Zeke
        User zeke  = (User) dao.getById(4);
        
        //clean pavilion if it is not set yet
        //only for dummy demo
        if(hsr.getSession().getAttribute("setPavilion")==null){
        user.setPavilion(null);
        //dummy
        james.setPavilion(null);
        zeke.setPavilion(null);
        hsr.getSession().setAttribute("setPavilion", true);}
        //would be set to null if the program runs again
        Set set = null;
        
        //Put user into a pavilion
        Pavilion pgroup;
        if(hsr.getSession().getAttribute("pgroup")==null){
        pgroup = new Pavilion();
        hsr.getSession().setAttribute("pgroup",pgroup);
        }else{
            pgroup = (Pavilion)hsr.getSession().getAttribute("pgroup");
        }
        pgroup.setDateCreated(null);
        pgroup.setDescription(pDescriptiono);
        pgroup.setName(pName);
        try{
        //start from an empty group
        if(pgroup.getUsers().isEmpty()){
            //?? we have to use HashSet here
            set = new HashSet<User>();
            if(user.getPavilion()==null){
            set.add(user);
            //
            set.add(james);
            set.add(zeke);
            //add user to pavilion group only once
            user.setPavilion(pgroup);}
        }else{
            set = pgroup.getUsers();
            if(user.getPavilion()==null){
            set.add(dao.getById(1));
            //
            set.add(dao.getById(3));
            set.add(dao.getById(4));
            user.setPavilion(pgroup);
            james.setPavilion(pgroup);
            zeke.setPavilion(pgroup);
            }
        }
            pgroup.setUsers(set);
           
        }catch(Exception e){}
        dao.saveOrUpdate(pgroup);
        //update user pavilion info
        dao.saveOrUpdate(user);
        hsr.getSession().setAttribute("pgroup", pgroup);
        
        //Creating a Leader Board
        set = null;
        Leaderboard lb;
        if(hsr.getSession().getAttribute("lb")==null){
        lb = new Leaderboard();
        hsr.getSession().setAttribute("lb", lb);
        }else{
            lb = (Leaderboard)hsr.getSession().getAttribute("lb");
        }
        
        if(lb.getPavilions().isEmpty()){
            set = new HashSet<Pavilion>();
            set.add(pgroup);
        }else{
            set = lb.getPavilions();
            set.add(pgroup);
        }
        //saving updates
        lb.setPavilions(set);
        dao.saveOrUpdate(lb);
        hsr.getSession().setAttribute("lb", lb);
        //counting points
        //get Two Teams
        LinkedList<Player> teamA;
        LinkedList<Player> teamB;
        if(hsr.getSession().getAttribute("makeTeams")==null){
        teamA = makeTeamA();
        teamB = makeTeamB();
        hsr.getSession().setAttribute("teamA",teamA);
        hsr.getSession().setAttribute("teamB",teamB);
        hsr.getSession().setAttribute("makeTeams",true);
        
        }else{
            teamA = (LinkedList<Player>)hsr.getSession().getAttribute("teamA");
            teamB = (LinkedList<Player>)hsr.getSession().getAttribute("teamB");            
            teamA = updatePoints(teamA);
            teamB = updatePoints(teamB);
            hsr.getSession().setAttribute("teamA",teamA);
            hsr.getSession().setAttribute("teamB",teamB);
        }
        
        
        int count = 0;
        String selected = hsr.getParameter("selected");
        
        //set session
        if((selected!=null&&selected!="")){
            hsr.getSession().setAttribute("selected",selected);
        }
        
        //assign points for user
        if(hsr.getSession().getAttribute("selected")!=null){
        selected = (String)hsr.getSession().getAttribute("selected");
        for(Player a : teamA){
            a.setSelected(0);
            if(a.getName().equals(selected)){
                count = a.getPoints();
                a.setSelected(1);
            }
        }
        
        for(Player b : teamB){
            b.setSelected(0);
            if(b.getName().equals(selected)){
                count = b.getPoints();
                b.setSelected(1);
            }
        }
        }
        mv.addObject(user);
        mv.addObject("message",count);
        
        //Get the Pavilion into a User 
        return mv;
    }
        
    public LinkedList<Player> makeTeamA(){
        LinkedList<Player> listA = new LinkedList();
        Player a = new Player();
        Player b = new Player();
        Player c = new Player();
        a.setName("Bruno Caboclo");
        b.setName("Bismack Biyombo");
        c.setName("Delon Wright");
        listA.add(a);
        listA.add(b);
        listA.add(c);
        return  listA;
    }
    
    public LinkedList<Player> makeTeamB(){
        LinkedList<Player> listB = new LinkedList();
        Player a = new Player();
        Player b = new Player();
        Player c = new Player();
        a.setName("Patrick Patterson");
        b.setName("James Johnson");
        c.setName("DeMarre Carroll");
        listB.add(a);
        listB.add(b);
        listB.add(c);
    return listB;
    }
    
    public LinkedList<Player> updatePoints(LinkedList<Player> team){
        int count = 0;
        for(Player p : team){
            count = count + 1;
            p.setPoints(p.getPoints()+count);
        }
        return team;
    }
}
