/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import entities.Gamecards;
import entities.User;
import java.util.HashSet;
import java.util.Set;
import model.BaseDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author ma
 */
@Controller
public class GameController {
    @Autowired private BaseDAO dao;
    
    @RequestMapping(value="game.htm", method = RequestMethod.GET)
    public String load(){
        User user = (User) dao.getById(1);
        Gamecards card = new Gamecards();
        card.setName("Super Zeke");
        Set set = null;
        set = new HashSet();
        set.add("Kobe");
        card.setPlayerses(set);
        card.setUser(user);
        return "/game/gaming";
    }
}
