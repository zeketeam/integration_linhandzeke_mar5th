/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import entities.User;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.HibernateUtil;
import model.UserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Ziyi
 */
@Controller
public class UserProfileController {
    @Autowired private UserDAO userDAO;
    
    public void setUserDAO(UserDAO userDAO) {
            this.userDAO = userDAO;
    }
    
    @ModelAttribute("updatedUser")
    public User getUserObject(){
        return new User();
    }
    @RequestMapping(value="/userProfileIndex.htm", method = RequestMethod.GET)
    public String userProfileIndexHandler(HttpServletRequest hsr, 
            HttpServletResponse hsr1,
            ModelMap modelMap) throws Exception {
        ModelAndView mv = new ModelAndView("userProfileIndex");
        String out = "";
        User currentUser=null;
        try {
                currentUser = (User)hsr.getSession().getAttribute("user");               
            } 
        catch (Exception e) { 
                System.err.println("===Zeke!!!SearchError");
                e.printStackTrace();
            }
        modelMap.put("currentUser", currentUser);
        
        mv.addObject("message", out);
        
        return "userProfile/userProfileIndex";
    }
    
    @RequestMapping(value="/userProfileEdit.htm", method = RequestMethod.POST)
    public String userProfileEditHandler(
            HttpServletRequest req, 
            HttpServletResponse res,
            ModelMap modelMap) throws Exception {
        ModelAndView mv = new ModelAndView("userProfileEdit");
        String out = "";
        User currentUser=null;
        try {               
                currentUser = (User)req.getSession().getAttribute("user");               
            } 
        catch (Exception e) { 
                System.err.println("===Zeke!!!SearchError");
                e.printStackTrace();
            }
        modelMap.addAttribute("currentUser", currentUser);
                
        mv.addObject("message", out);
        
        return "userProfile/userProfileEdit";
    }
    
    @RequestMapping(value="/userProfileResult.htm", method = RequestMethod.POST)
    public String userProfileResultHandler(HttpServletRequest req,
            ModelMap modelMap, 
            User user) throws Exception {
        ModelAndView mv = new ModelAndView("userProfileEdit");
        String out = "";

        User currentUser=null;
        try {               
                currentUser = (User)req.getSession().getAttribute("user");    
                user.setId(currentUser.getId());
                user.setPwd(DigestUtils.md5DigestAsHex(user.getPwd().getBytes()));
                user.setStatus("1");
                user.setActive("1");
                userDAO.saveOrUpdate(user);               
                currentUser = (User)userDAO.getById(currentUser.getId());
                req.getSession().setAttribute("user", currentUser);
            } 
        catch (Exception e) { 
                System.err.println("===Zeke!!!SearchError");
                e.printStackTrace();
            }
        out = "User profile updated.";
        mv.addObject("message", out);
        modelMap.addAttribute("currentUser", currentUser);
        modelMap.addAttribute("msg", out);
        return "userProfile/userProfileIndex";
    }
}
