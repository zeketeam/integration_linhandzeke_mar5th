<%-- 
    Document   : activationConfirm
    Created on : 27-Feb-2016, 5:42:57 AM
    Author     : Linh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Activation Confirm</title>
    </head>
    <body>
        <div id="wrapper">
        <jsp:include page="../include/header.jsp" />
        <main>
            <div id="template">
                <h3>Your activation is successful.</h3>
            </div>
        </main>
        <jsp:include page="../include/footer.jsp" />
        </div>
    </body>
</html>
