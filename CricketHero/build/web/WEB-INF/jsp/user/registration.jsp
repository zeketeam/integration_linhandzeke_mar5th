<%-- 
    Document   : registration
    Created on : 21-Feb-2016, 12:26:02 AM
    Author     : Linh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registration</title>
    </head>
    <body>
        <div id="wrapper">
        <jsp:include page="../include/header.jsp" />
        <main>
            <form:form action="registrationSuccess.htm" commandName="user" method="post">
            <div class="error">    ${error} </div>
            <table>
                    <tr>
                            <td>User Name :</td>
                            <td><form:input path="email" size="40"/></td>
                    </tr>
                    <tr>
                            <td>Password :</td>
                            <td><form:password path="pwd" size="40"/></td>
                    </tr>
                    <tr>
                            <td>Retype Password :</td>
                            <td><input type="password" name="retypePassword" size="40"/> </td>
                    </tr>
                    <tr>
                            <td>Name :</td>
                            <td><form:input path="name" size="40"/></td>
                    </tr>
                    <tr>
                            <td>Address :</td>
                            <td><form:input path="address" size="40"/></td>
                    </tr>
                    <tr>
                            <td>Phone :</td>
                            <td><form:input path="phone" size="40"/></td>
                    </tr>
                    <tr height="50">
                        <td colspan="2"><input type="checkbox" name="disclaimer" value="disclaimer"> Disclaimer<br></td>
                    </tr>

                    <tr>
                        <td></td>
                        <td><input type="submit" value="Register"></td>
                    </tr>
            </table>
            </form:form>
        </main>
        <jsp:include page="../include/footer.jsp" />
        </div>
    </body>
</html>
