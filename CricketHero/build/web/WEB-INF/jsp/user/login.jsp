<%-- 
    Document   : login
    Created on : 21-Feb-2016, 1:45:20 PM
    Author     : Linh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="resources/css/userpage.css" type="text/css" rel="stylesheet"/>
        <title>LOGIN</title>
    </head>
    <body>
        
        <div id="wrapper">
        <jsp:include page="../include/header.jsp" />
        <main>
            <form:form action="loginConfirm.htm" commandName="user" method="post">
            <div class="error">    ${error} </div>
            <table>
                <tr>
                    <td>User Name :</td>
                    <td><form:input path="email" maxlength="100" size="40" /></td>
                </tr>
                <tr>
                    <td>Password :</td>
                    <td><form:password path="pwd" size="40" /></td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <input type="submit" name="login" value="Login"/>&nbsp;
                        <button type="submit" formaction="signup.htm">Sign Up</button>
                    </td>
                </tr>
                 <tr>
                    <td></td>
                    <td>
                        <a href="forgetpwd.htm">Forget your password?</a>
                    </td>
                </tr>
            </table>
            
            </form:form>
        </main>
        <jsp:include page="../include/footer.jsp" />
        </div>
    </body>
</html>
