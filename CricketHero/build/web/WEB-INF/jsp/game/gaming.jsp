<%-- 
    Document   : gameboard
    Created on : 27-Feb-2016, 12:38:40 AM
    Author     : ma
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Gaming</title>
          <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
        
        <script type="text/javascript">
           function executeQuery() {
           $('#leaderboard').load("${pageContext.request.contextPath}/leaderboard.htm");
          } 
            $(document).ready(function() {
                // run the first time; all subsequent calls will take care of themselves
                executeQuery();
                setInterval(executeQuery, 5000);
                //$('#leaderboard').setInterval(function(){},3000);
              });
        </script>
        <link rel="stylesheet" type="text/css" href="resources/css/bootstrap.min.css" />
    </head>
    <body>
        <div id="wrapper" class="row col-md-12">
        <jsp:include page="../include/header.jsp" />
        <main>
             <div id="gameFrame" class="col-md-6 text-center">
                
                 <iframe width="420" height="315"
                         src="http://www.youtube.com/embed/YyuMUNqTBs0?autoplay=1">
                </iframe>
                  <select>
                     <option>Series 1</option>
                     <option>Series 2</option>
                     <option>Series 3</option>
                 </select>
             </div>
            <!--http://www.youtube.com/embed/XGSy3_Czz8k?autoplay=1-->
            <div id="leaderboard" class="col-md-6 text-center">
                  
            </div>
               
               

            

            
        </main>
        <jsp:include page="../include/footer.jsp" />
        </div>
    </body>
</html>
